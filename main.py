from fastapi import FastAPI, HTTPException
from fastapi.responses import RedirectResponse
from pydantic import BaseModel
from typing import List
import logging
from contextlib import asynccontextmanager

from logger import setup_logging
from llm_init import initialize_llm
from file_operations import find_files, aggregate_file_contents
from analysis import analyze, format_results, track_criteria

logger = logging.getLogger(__name__)


class AnalysisRequest(BaseModel):
    directory: str
    track: str
    patterns: List[str]


@asynccontextmanager
async def lifespan(app: FastAPI):
    # Startup
    setup_logging()
    logger.info("Starting FastAPI application.")
    yield
    # Shutdown
    logger.info("Shutting down FastAPI application.")


app = FastAPI(lifespan=lifespan)


@app.get("/")
async def redirect_root_to_docs():
    return RedirectResponse("/docs")


@app.post("/analyze")
async def perform_analysis(request: AnalysisRequest):
    logger.info("Starting analysis request.")

    valid_tracks = {"BDT", "FDT", "QA", "GENERIC"}
    track = request.track.upper()
    if track not in valid_tracks:
        logger.error("Invalid track specified: %s", request.track)
        raise HTTPException(status_code=400,
                            detail=f"Invalid track specified: {request.track}. Must be one of {', '.join(valid_tracks)}.")

    found_files = find_files(request.directory, request.patterns)
    if not found_files:
        logger.warning("No files found matching patterns %s in directory %s", request.patterns, request.directory)
        raise HTTPException(status_code=404,
                            detail=f"No files found matching patterns {request.patterns} in directory {request.directory}")

    llm = initialize_llm()
    aggregated_content = aggregate_file_contents(found_files)
    results = analyze(aggregated_content, track, llm)
    formatted_results = format_results(results, llm)

    logger.info("Analysis complete.")
    return formatted_results
    return results


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
