import json
import logging
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.embeddings import GPT4AllEmbeddings
from langchain_community.vectorstores import Chroma
from langchain.chains import RetrievalQA
from chromadb.config import Settings

logger = logging.getLogger(__name__)


def validate_and_correct_json(json_string, llm):
    logger.info("Validating and correcting JSON.")
    try:
        # Try to load the JSON string to see if it's valid
        json_data = json.loads(json_string)
        logger.info("JSON is valid.")
        return json_data
    except json.JSONDecodeError as e:
        logger.warning("Invalid JSON detected, attempting to correct. Error: %s", e)
        # Define a query to correct the JSON
        query = (
            "Correct the following JSON to make it valid. If there are any issues, "
            "fix them and return the corrected JSON: "
        )
        query += json_string

        # Query the LLM to correct the JSON
        try:
            response = llm.invoke(str(query))
        except Exception as e:
            logger.error("Error occurred while invoking LLM: %s", e)
            raise ValueError("Error occurred while invoking LLM")

        # Make sure response is a dictionary and contains the expected key
        if not isinstance(response, dict) or 'result' not in response:
            logger.error("Invalid response from LLM: %s", response)
            raise ValueError("Invalid response from LLM")

        try:
            corrected_json_data = json.loads(response['result'])
            logger.info("JSON corrected successfully.")
            return corrected_json_data
        except json.JSONDecodeError as e:
            logger.error("Failed to correct the JSON. Error: %s", e)
            raise ValueError("Invalid JSON response from LLM even after correction attempt")


def track_criteria(track):
    logger.info("Determining criteria for track: %s", track)
    generic_criteria = [
        "Maintainability", "Object Modelling", "Readability", "Correctness",
        "Magic Numbers", "Compact Methods", "Expressive Code", "Code Duplication",
        "Build Process",
    ]

    specific_criteria = {
        "BDT": [
            "OOPS principles", "Encapsulation", "Unit Tests/Test Coverage",
        ],
        "FDT": [],
        "QA": [
            "OOPS principles", "Encapsulation", "Unit Tests/Test Coverage",
        ]
    }

    criteria = generic_criteria + specific_criteria.get(track, [])
    logger.info("Criteria for track %s: %s", track, criteria)
    return ", ".join(criteria)


def analyze(content, track, llm):
    logger.info("Starting analysis.")
    try:
        text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=100)
        all_splits = text_splitter.split_text(content)
    except Exception as e:
        logger.error("Error occurred while splitting text: %s", e)
        raise ValueError("Error occurred while splitting text")

    try:
        logger.info("Loading embeddings model.")
        model_name = "all-MiniLM-L6-v2.gguf2.f16.gguf"
        gpt4all_kwargs = {'allow_download': 'True'}
        embeddings = GPT4AllEmbeddings(model_name=model_name, gpt4all_kwargs=gpt4all_kwargs)
    except Exception as e:
        logger.error("Error occurred while loading embeddings model: %s", e)
        raise ValueError("Error occurred while loading embeddings model")

    try:
        logger.info("Creating vector store.")
        vectorstore = Chroma.from_texts(
            all_splits,
            embedding=embeddings,
            collection_name="vectorstore",
            persist_directory="db",
            client_settings=Settings(anonymized_telemetry=False)
        )
        qachain = RetrievalQA.from_chain_type(llm, retriever=vectorstore.as_retriever())
    except Exception as e:
        logger.error("Error occurred while creating vector store: %s", e)
        raise ValueError("Error occurred while creating vector store")

    combined_response = {'criteria': []}
    criteria_list = track_criteria(track).split(',')

    logger.info("Querying LLM for analysis.")
    for criterion in criteria_list:
        query = (
            f"Evaluate the overall code quality of the provided project based on the following criterion: {criterion}. "
            "Provide a detailed report and rate the criterion from A (best) to E (worst) or A+ to E+. "
            "Do not use ratings such as A- or B- or C- or D- or E-. "
            "If a rating is not applicable or not possible, show NA. "
            "Identify the build system used in the project "
            "(e.g., if there is a file called build.gradle: gradle, pom.xml: maven, package.json: nodejs, etc). "
            "Only check for the specified criterion; DO NOT ADD ANY OTHER CRITERIA. "
            "Include references to specific parts of the code where applicable. "
            "Always provide the full path to the file in references. "
            "In excerpts, do not provide the whole code; use '...' to mark continuation. "
            "Do not output anything except the JSON asked in format"
            "Report should be in the following JSON format: \n"
            "{\n"
            "  'criteria': [\n"
            "    {\n"
            f"      'name': '{criterion}',\n"
            "      'rating': '<Rating>',\n"
            "      'explanation': '<Explanation>',\n"
            "      'references': [\n"
            "        {\n"
            "          'file': '<Full Path to Filename. Do not start filenames with '.' or '/'>',\n"
            "          'excerpt': '<Code Excerpt with ... for continuation. Each excerpt should be a maximum of 2-3 lines long. >'\n"
            "        }\n"
            "      ]\n"
            "    }\n"
            "  ]\n"
            "}"
        )

        logger.info(f"Querying LLM for criterion: {criterion}")
        try:
            response = qachain.invoke({"query": query, "context": all_splits})
        except Exception as e:
            logger.error("Error occurred while querying LLM: %s", e)
            raise ValueError("Error occurred while querying LLM")

        # Validate and correct JSON response
        try:
            response_json = validate_and_correct_json(response['result'], llm)
            combined_response['criteria'].append(response_json['criteria'])
        except Exception as e:
            logger.error("Error occurred while validating and correcting JSON: %s", e)
            raise ValueError("Error occurred while validating and correcting JSON")

    logger.info("Analysis complete.")
    return combined_response


def format_results(results, llm):
    logger.info("Formatting results.")

    allowed_ratings = {"A", "B", "C", "D", "E", "A+", "B+", "C+", "D+", "E+", "NA"}

    # Define a query to validate and normalize the ratings
    query = (
        f"Validate the following JSON and refactor as needed to make the JSON valid. "
        f"The JSON should only contain the following ratings: {allowed_ratings}. "
        "If not, normalize the ratings. Here is the JSON to validate: "
    )

    # Combine the query with the actual results
    query += json.dumps(results, indent=2)

    logger.info("Querying LLM for validation and normalization.")
    try:
        response = llm.invoke({"query": query})
    except Exception as e:
        logger.error("Error occurred while invoking LLM: %s", e)
        raise ValueError("Error occurred while invoking LLM")

    # Validate and correct JSON response
    try:
        formatted_results = validate_and_correct_json(response['result'], llm)
    except Exception as e:
        logger.error("Error occurred while validating and correcting JSON: %s", e)
        raise ValueError("Error occurred while validating and correcting JSON")

    logger.info("Results formatted successfully.")
    return formatted_results
