import logging
from langchain_community.llms import Ollama
from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler

logger = logging.getLogger(__name__)


def initialize_llm():
    MODEL="phi3:mini"
    logger.info(f"Initializing LLM with model '{MODEL}'.")
    return Ollama(
        model=MODEL,
        temperature=0,
        callback_manager=CallbackManager([StreamingStdOutCallbackHandler()])
    )
