# CodeQuality Checker

### Usage
##### Setup Ollama
```bash
ollama pull phi3:mini
ollama serve
```

##### Start the server
```commandline
python main.py
```

##### Example curl request
```commandline
curl -s -X POST "http://localhost:8000/analyze" \
            -H "Content-Type: application/json" \
            -d '{
              "directory": "ME_QA_XFLIX_MODULE_L1_SOLUTION",
              "track": "QA",
              "patterns": ["*.java", "*.gradle"]
            }' | jq
```
#### Preview
[preview](https://gitlab.com/saroj-criodo/codequality/-/raw/main/preview.gif)
