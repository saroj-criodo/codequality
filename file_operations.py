import os
import fnmatch
import logging
from langchain_community.document_loaders import TextLoader

logger = logging.getLogger(__name__)


def find_files(directory, patterns):
    logger.info("Searching for files in directory '%s' with patterns: %s", directory, patterns)
    found_files = []
    for root, _, files in os.walk(directory):
        for file in files:
            for pattern in patterns:
                if fnmatch.fnmatch(file, pattern):
                    found_files.append(os.path.join(root, file))
                    break
    logger.info("Found %d files: %s", len(found_files), found_files)
    return found_files


def load_file(file_path):
    logger.info("Loading file: %s", file_path)
    loader = TextLoader(file_path=file_path, encoding="utf-8")
    return loader.load()


def aggregate_file_contents(file_paths):
    logger.info("Aggregating content from %d files.", len(file_paths))
    aggregated_content = ""
    for file_path in file_paths:
        file_data = load_file(file_path)
        file_content = "\n".join([document.page_content for document in file_data])
        aggregated_content += f"File: {file_path}\n{file_content}\n\n"
    logger.info("Content aggregation complete.")
    return aggregated_content
